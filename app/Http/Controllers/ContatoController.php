<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;

class ContatoController extends Controller
{
    public function index(){
    	$data['titulo']= "Contato";

    	return view('contato', $data);
    }

    public function enviar(Request $request){
    	$this->validate($request,[
    		'nome' => 'required|min:3',
    		'email' => 'required|email',
    		'assunto' => 'required|min:3',
    		'msg' => 'required|min:10'
    	]);

    	$dadosEmail = array(
    		'nome' => $request->input('nome'),
    		'email' => $request->input('email'),
    		'assunto' => $request->input('assunto'),
    		'msg' => $request->input('msg')
    	);

    	Mail::send('email.contato',$dadosEmail,function($message){
    		$message->from('dayane@webdl.com.br','Formulário de Contato');
    		$message->subject('Mensagem enviada pelo formulário de contato');
    		$message->to('dayane@webdl.com.br')->cc('dayane_lustosa@hotmail.com');
    	});

    	return redirect('contato')->with('success','Mensagem enviada com sucesso, em breve entraremos em contato!');
    }
}

