<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<title>Nosso Site - @yield('title')</title>
	<link rel="stylesheet" type="text/css" href="{{URL::to('dist/css/booststrap.min.css')}}">
</head>
<body>
	<div class="container">
		@yield('content')
	</div>
	<script type="text/javascript" src="{{URL::to('js/jquery.min.js')}}"></script>
	<script type="text/javascript" src="{{URL::to('dist/js/booststrap.min.js')}}"></script>
</body>
</html>